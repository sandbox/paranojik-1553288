<?php

/**
 * @file
 * Helper functions to make theming easier.
 */

/**
 * Display the view as an expandable table.
 *
 * @see template_preprocess_views_view_table()
 */
function template_preprocess_views_collapsible_table_plugin_style(&$vars) {
  // Preprocess the results as if it were normal table.
  template_preprocess_views_view_table($vars);

  $module_path = drupal_get_path('module', 'views_collapsible_table');

  // Send base stylesheets to the page.
  drupal_add_js($module_path .'/js/views_collapsible_table.js');
  $settings = array(
    'expand_link_text' => $vars['options']['expand_link_text'],
    'collapse_link_text' => $vars['options']['collapse_link_text'],
  );
  drupal_add_js(array('views_collapsible_table' => $settings), 'setting');

  // Split the rows array into two parts - cannot user array_splice here because it does not preserve keys
  $vars['rows_collapsible'] = array_slice($vars['rows'], $vars['options']['initially_visible_rows'], NULL, TRUE);
  $vars['rows'] = array_diff_key ($vars['rows'], $vars['rows_collapsible']);
}