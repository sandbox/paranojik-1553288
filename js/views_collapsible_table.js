/**
 * @file
 * views_collapsible_table javascript functions.
 */
(function ($) {
  /**
   * Collapse part of the table and expand it back
   */
  Drupal.behaviors.views_collapsible_table = {};
  Drupal.behaviors.views_collapsible_table.attach = function(context) {
    $('.table-expand-button').click(function() {
      var button = $(this);
      if ($(this).hasClass('expanded')) {
        $(this).parent().prev().find('.collapsible-part').slideToggle('slow', function() {
          button.removeClass('expanded');
          button.html(Drupal.settings.views_collapsible_table.expand_link_text);
          $('html, body').animate({
            scrollTop: $(this).parent().find('.visible-part').offset().top
          }, 100);
        });
      }
      else {
        $(this).parent().prev().find('.collapsible-part').slideToggle('slow', function() {
          button.addClass('expanded');
          button.html(Drupal.settings.views_collapsible_table.collapse_link_text);
        });
      }
      return false;
    });
  }
})(jQuery);


