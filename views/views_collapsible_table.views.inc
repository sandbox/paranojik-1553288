<?php

/**
 * @file
 * Integration with Views.
 */

/**
 * Implementation of hook_views_plugins().
 */
function views_collapsible_table_views_plugins() {
  $module_path = drupal_get_path('module', 'views_collapsible_table');
  return array(
    'module' => 'views_collapsible_table', // This just tells our themes are elsewhere.
    'style' => array(
      'collapsible_table' => array(
        'title' => t('Collapsible table'),
        'help' => t('Displays items in a table with a collapsible row group.'),
        'handler' => 'views_collapsible_table_plugin_style',
        'path' => $module_path .'/views',
        'theme path' => $module_path .'/theme',
        'theme file' => 'theme.inc',
        'theme' => 'views_collapsible_table_plugin_style',
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'uses grouping' => TRUE,
        'type' => 'normal',
        'help topic' => 'style-collapsible-table',
      ),
    ),
  );
}