<?php

/**
 * @file
 * Contains the collapsible table style plugin.
 */

/**
 * Style plugin to render the view in a table with a collapsible group of rows.
 *
 * @ingroup views_style_plugins
 */
class views_collapsible_table_plugin_style extends views_plugin_style_table {
  /**
   * Set default options.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['initially_visible_rows'] = array('default' => 4);
    $options['expand_link_text'] = array('default' => t('Expand table'));
    $options['collapse_link_text'] = array('default' => t('Collapse table'));
    $options['initial_state'] = array('default' => 'collapsed');
    return $options;
  }

  /**
   * Provide a form to edit options for this plugin.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['initially_visible_rows'] = array(
      '#type' => 'textfield',
      '#title' => t('Visible rows when collapsed'),
      '#default_value' => $this->options['initially_visible_rows'],
    );

    $form['expand_link_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Expand link text'),
      '#default_value' => $this->options['expand_link_text'],
    );

    $form['collapse_link_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Collapse link text'),
      '#default_value' => $this->options['collapse_link_text'],
    );

    $form['initial_state'] = array(
      '#type' => 'radios',
      '#title' => t('Initial state'),
      '#options' => array('collapsed' => t('Collapsed'), 'expanded' => t('Expanded')),
      '#default_value' => $this->options['initial_state'],
    );
  }  
}